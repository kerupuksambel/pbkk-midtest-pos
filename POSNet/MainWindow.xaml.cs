﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace POSNet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public int total = 0;
        public MainWindow()
        {
            InitializeComponent();
        }


        private void btnFinish_click(object sender, System.EventArgs e)
        {
            MessageBox.Show("hehe");
        }

        private void txtID_keydown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                string[] barang = Get_Barang(txtID.Text, txtJumlah.Text);
                if(barang[0] == "")
                {
                    MessageBox.Show("ID tidak diketahui atau jumlah Anda salah.");
                }
                else
                {
                    var item = new POSItem { Nama = barang[0], Harga = int.Parse(barang[1]), Jumlah = int.Parse(txtJumlah.Text), Subtotal = int.Parse(barang[3]) };
                    dgRecord.Items.Add(item);
                    total += item.Subtotal;
                    lblTotal.Content = total.ToString();
                }
            }
        }

        private void btnReset_click(object sender, System.EventArgs e)
        {
            dgRecord.Items.Clear();
            dgRecord.Items.Refresh();
            total = 0;
            lblTotal.Content = total.ToString();
        }

        private void btnCheckout_click(object sender, System.EventArgs e)
        {
            CheckoutWindow cw = new CheckoutWindow();
            cw.lblTotal.Content = total.ToString();
            cw.Show();
            dgRecord.Items.Clear();
            dgRecord.Items.Refresh();
            total = 0;
            lblTotal.Content = total.ToString();
        }

        public string[] Get_Barang(string ID, string jumlah)
        {
            var connection = new MySqlConnection("Server=localhost;Database=net_pos;Uid=root;Pwd=;");
            connection.Open();
            var command = new MySqlCommand("SELECT * FROM barang WHERE id = '" + ID + "'", connection);
            var reader = command.ExecuteReader();
            string[] result = {"", "", ""};
            while (reader.Read())
            {
                int jumlahInt;
                bool parsable = int.TryParse(jumlah, out jumlahInt);
                if (parsable)
                {
                    int total = jumlahInt * int.Parse(reader["harga"].ToString());
                    return new string[] { reader["nama"].ToString(), reader["harga"].ToString(), reader["stok"].ToString(), total.ToString() };
                }
            }

            return result;
        }
    }

    public class POSItem
    {
        public string Nama { get; set; }
        public int Harga { get; set; }
        public int Jumlah { get; set; }
        public int Subtotal { get; set; }
    }
}


