﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POSNet
{
    public partial class CheckoutWindow : Window
    {
        public CheckoutWindow()
        {
            InitializeComponent();
        }

        private void txtPembayaran_KeyDown(object sender, KeyEventArgs e)
        {
            int cash;
            bool parsable = int.TryParse(txtPembayaran.Text, out cash);
            if (parsable)
            {
                int kembalian = cash - int.Parse(lblTotal.Content.ToString());
                lblKembalian.Content = kembalian.ToString();
                if(kembalian < 0)
                {
                    btnSelesai.IsEnabled = false;
                }
                else
                {
                    btnSelesai.IsEnabled = true;
                }
            }
            else
            {
                MessageBox.Show("Jumlah kembalian tidak valid.");
            }
        }

        private void btnSelesai_Click(object sender, RoutedEventArgs e)
        {
            winCheckout.Close();
        }
    }
}
