# Point of Sales with .NET and WPF

## Requirement
- mySQL
- Visual Studio (untuk develop)

## Cara Pemakaian
- Import `net_pos.sql` ke dalam database MySQL Anda
- Buka project, dan di function `Get_Barang`, cari line `var connection = ...` dan ganti isinya dengan format `Server=[SERVER];Database=[NAMA DATABASE];Uid=[USERNAME];Pwd=[PASSWORD]`
- Run project
